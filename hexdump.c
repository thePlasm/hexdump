#include <stdio.h>

#define FIRST_GRAPHICAL_CHAR 32
#define LAST_GRAPHICAL_CHAR 126

int main(argc, argv)
int argc;
char **argv;
{
	FILE *filePtr;
	int c;
	int pos = 0;
	unsigned char line[16];
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 1;
	}
	if ((filePtr = fopen(argv[1], "r")) == NULL) {
		fprintf(stderr, "Error opening file\n");
		return 1;
	}
	while ((c = getc(filePtr)) != EOF) {
		char posX = pos & 15;
		if (!posX && pos) {
			char i;
			printf("0x%08X\t", pos - 16);
			for (i = 0; i < 16; i++)
				printf("%02X%c", line[i], i == 15 ? '\t' : ' ');
			for (i = 0; i < 16; i++)
				putchar(line[i] >= FIRST_GRAPHICAL_CHAR && line[i] <= LAST_GRAPHICAL_CHAR ? line[i] : '.');
			putchar('\n');
		}
		line[posX] = c;
		pos++;
	}
	fclose(filePtr);
	if (pos & 15) {
		char i;
		printf("0x%08X\t", pos - (pos & 15));
		for (i = 0; i < 16; i++)
			if (i < (pos & 15))
				printf("%02X%c", line[i], i == 15 ? '\t' : ' ');
			else
				printf("  %c", i == 15 ? '\t' : ' ');
		for (i = 0; i < (pos & 15); i++)
			putchar(line[i] >= FIRST_GRAPHICAL_CHAR && line[i] <= LAST_GRAPHICAL_CHAR ? line[i] : '.');
		putchar('\n');
	}
	return 0;
}
